﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float padding;
    [SerializeField] float speedBullet;
    [SerializeField] float fireRate=0.1f;
    

    [SerializeField] GameObject bullet;

    Vector3 xyMin;
    Vector3 xyMax;

    float nextFire;
    // Start is called before the first frame update
    void Start()
    {
        Camera gameCamera = Camera.main;
        xyMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0));
        xyMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }

    // Update is called once per frame
    void Update()
    {

        Move();
        Shoot();
    }
    void Move()
    {
        
        
        Vector3 currentPos = transform.position;
            float inputY = Input.GetAxis("Vertical");
         float inputX = Input.GetAxis("Horizontal");
        transform.position = new Vector3(currentPos.x+ inputX * speed * Time.deltaTime, currentPos.y +inputY *speed * Time.deltaTime, currentPos.z);
        float newX= Mathf.Clamp( transform.position.x, xyMin.x+padding, xyMax.x-padding);
        float newY= Mathf.Clamp( transform.position.y, xyMin.y+ padding, xyMax.y- padding);
        transform.position = new Vector3(newX, newY, 0);



    }
    void Shoot()
    {
        if (Input.GetButton("Fire1")&& Time.time>=nextFire)
        {
          GameObject newBullet=  Instantiate(bullet, transform.position, Quaternion.identity);
            nextFire = Time.time + fireRate;

        }

    }

   /* private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageDealer damageDealer = collision.GetComponent<DamageDealer>();
        if (damageDealer != null)
        {
            health -= damageDealer.GetDamage();
        }
    }*/
}
