﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
   List<Transform> waypoints;
    float speed;
    int currentWayPointIndex;

    public void SetWayPoints(List<Transform> waypoints)
    {
        this.waypoints = waypoints;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    // Start is called before the first frame update
    void Start()
    {

        transform.position = waypoints[0].position;
        currentWayPointIndex = 1;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        Vector3 targetPosition = waypoints[currentWayPointIndex].position;
        float movementDelta = speed * Time.deltaTime;
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, movementDelta);
        Debug.Log("1");
        if (Mathf.Approximately(targetPosition.x,transform.position.x)&& Mathf.Approximately(targetPosition.y, transform.position.y)) 
        {

            if (currentWayPointIndex == waypoints.Count - 1)
            {
                Destroy(gameObject);
            }

            else
            {
                currentWayPointIndex++;
            }
        }
    }
}
