﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    [SerializeField] List<WayConfig> waves;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnWavesAll());
    }

    IEnumerator SpawnWavesAll()
    {
        for (int waveIndex=0; waveIndex < waves.Count; waveIndex++)
        {
            yield return StartCoroutine(SpawnEnemies(waves[waveIndex]));
        }
    }
    IEnumerator SpawnEnemies(WayConfig wayConfig)
    {
        for (int i = 0; i < (wayConfig.GetEnemysNumber()); i++)
        {
            GameObject newEnemy = Instantiate(waves[0].GetEnemy());
            EnemyMovement enemyMovement = newEnemy.GetComponent<EnemyMovement>();
            enemyMovement.SetSpeed(waves[0].GetEnemySpeed());
            enemyMovement.SetWayPoints(waves[0].GetWayPoints());
       
            yield return new WaitForSeconds(waves[0].GetSpawnRate());
        }

    }
}
