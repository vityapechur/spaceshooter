﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Way Config")]
public class WayConfig : ScriptableObject
{
    [SerializeField] GameObject path;
    [SerializeField] GameObject enemy;
    [SerializeField] float enemySpeed;
    [SerializeField] int  enemysNumber;
    [SerializeField] float spawnRate;

    public List<Transform> GetWayPoints()
    {
        List<Transform> waypoints= new List<Transform>();
        foreach (Transform child in path.transform)
        {
            waypoints.Add(child);
        }
        return waypoints;
    }

    public GameObject GetEnemy()
    { return enemy; }

    public float  GetEnemySpeed()
    { return enemySpeed; }

    public int GetEnemysNumber()
    { return enemysNumber; }

    public float GetSpawnRate()
    { return spawnRate; }



}
